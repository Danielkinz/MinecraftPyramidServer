local hol = require("component").hologram
local args = require("shell").parse(...)

local ccol = 0
local function getColor()
  return ccol
end
local function setColor(c)
  hol.setPaletteColor(1, c)
  ccol = c
end

local speed = tonumber(args[1]) or 3

setColor(0x00FF00)
local function iR(r,g,b, f)
  r = f(r)
  return r,g,b
end
local function iG(r,g,b, f)
  g = f(g)
  return r,g,b
end
local function iB(r,g,b, f)
  b = f(b)
  return r,g,b
end
local function add(n)
  n = n + speed
  return n
end
local function sub(n)
  n = n - speed
  return n
end
local inc = iR
local mode = 1

local function fromMode(r,g,b)
  local m = math.abs(mode)
  if m == 1 then
    inc = iR
    return r
  elseif m == 2 then
    inc = iG
    return g
  else
    inc = iB
    return b
  end
end
while true do
  local rgb = getColor()
  local r = (rgb >> 16) & 0xFF
  local g = (rgb >> 8) & 0xFF
  local b = rgb & 0xFF
  local toChange = fromMode(r,g,b)
  print(r,g,b,mode)
  if mode < 0 then
    while toChange > 0x0 do
      r,g,b = inc(r,g,b, sub)
      setColor(((r & 0xFF) << 16) | ((g & 0xFF) << 8) | (b & 0xFF))
      toChange = fromMode(r,g,b)
      os.sleep(0)
    end
    mode = -mode
    mode = mode + 1
    if mode > 3 then mode = 1 end
  else
    while toChange < 0xFF do
      r,g,b = inc(r,g,b, add)
      setColor(((r & 0xFF) << 16) | ((g & 0xFF) << 8) | (b & 0xFF))
      toChange = fromMode(r,g,b)
      os.sleep(0)
    end
    mode = mode + 1
    if mode > 3 then mode = 1 end
    mode = -mode
  end
end