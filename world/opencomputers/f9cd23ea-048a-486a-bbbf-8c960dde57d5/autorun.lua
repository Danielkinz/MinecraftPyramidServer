local filesystem = require("filesystem")
if not filesystem.exists("/home/provisioning_server.lua") then
  filesystem.copy("/mnt/f9c/provisioning_client.lua", "/home/provisioning_client.lua")
end