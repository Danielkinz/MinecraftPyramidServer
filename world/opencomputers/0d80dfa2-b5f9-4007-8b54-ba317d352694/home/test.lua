local component = require("component")
local event = require("event")
local fs = require("filesystem")

local function doNamedMount(name, address, componentType)
  if componentType == "filesystem" then
    local proxy = component.proxy(address)
    if proxy and proxy:getLabel() then
      local path = fs.concat("/mnt", (proxy:getLabel():gsub("[^%w_]+", "_")))
      if fs.exists(path) then
        -- Mount point exists already, start appending address
        local addrlen = 3
        local path = path .. "-" .. address:sub(1, addrlen)
        while fs.exists(path)
          and addrlen < address:len() -- just to be on the safe side
        do
          path = path .. address:sub(addrlen, addrlen)
        end
      end
      fs.mount(proxy, path)
    end
  end
end

function start(...)
  event.listen("component_added", doNamedMount)
end

-- Uncomment when placing this in /boot instead of using rc
-- start()