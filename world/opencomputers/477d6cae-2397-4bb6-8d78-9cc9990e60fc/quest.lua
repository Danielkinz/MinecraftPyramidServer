local component = require("component")
local qutil = require("qutil")
local xnet = component.xnet
local modem = component.modem
local os = require("os")
local reqs = {}
local cpos = {}
local inpos = nil

for i=1,16 do
  reqs[i] = qutil.get_reqs("./quests/" .. i .. ".txt").reqs
  local has_item = false
  for k,v in pairs(reqs[i]) do
    if v.type == "item" then
      has_item = true
      break
    end
  end
  if not has_item then reqs[i] = nil end  
end

local cb = xnet.getConnectedBlocks()
for i=1,16 do if reqs[i] ~= nil then
  local str = "Quest "..i
  for k,v in pairs(cb) do
    if type(v) == "table" and v.connector ~= nil then
      if v.connector == str or v.connector == str.." " then
        cpos[i] = v.pos
      elseif v.connector == "Input" then
        inpos = v.pos
      end
    end
  end
  if cpos[i] == nil then error("Could not find " .. str .. " chest!") end
end end

if inpos == nil then error("Could not find input chest!") end

function m_req(item, i, req, outId)
  if qutil.req_matches_item({cpt=xnet,side=inpos,slot=i,item=item},req) then
    xnet.transferItem(inpos,i,item.size,cpos[outId])
  end
end

while true do
  local x = xnet.getItems(inpos)
  for i=1,x.n do
    if x[i] ~= nil then
      for j=1,16 do
        if reqs[j] ~= nil then
          for k,v in pairs(reqs[j]) do
            if v.type == "item" then
              m_req(x[i],i,v,j)
            end
          end
        end
      end
      os.sleep(0.05)
    end
  end
  os.sleep(0.05)
end