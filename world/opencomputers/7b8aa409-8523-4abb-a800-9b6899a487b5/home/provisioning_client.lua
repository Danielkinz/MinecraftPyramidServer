local id = nil
local f = io.open("/home/id", "r")
id = f:read()
f:close()

local c_port = 30788
local c_sid = math.floor(math.random(0, 32767))

local shell = require("shell")
local component = require("component")
local event = require("event")
local computer=require("computer")
local serialization = require("serialization")
local m = component.modem
m.open(c_port)
m.setStrength(2048)
m.setWakeMessage("WAKE ME UP INSIDE")

-- configure reboot handler
event.listen("modem_message", function(name, localAddress, remoteAddress, port, distance, message)
  if c_port == math.floor(port) and message == "WAKE ME UP INSIDE" then
    computer.shutdown(true)
  end
end)

print(".-----------------------------------.")
print("|                                   |")
print("|  Quest Provisioning System v 1.0  |")
print("|                                   |")
print("'-----------------------------------'")
print("")
print("Registered as '" .. id .. "'")
print("")

local l_handler = nil
local l_found = false
local l_port = nil
local l_hdr = nil

l_handler = function(name, localAddress, remoteAddress, port, distance, message, param)
  if math.floor(port) == c_port and message == "PROVISIONING ACCEPT" then
    l_found = true
    if l_hdr ~= nil then
      l_hdr = serialization.unserialize(param)
    end
    event.ignore("modem_message", l_handler)
  elseif math.floor(port) == c_port and message == "PROVISIONING SHELL" then
    event.ignore("modem_message", l_handler)
    os.exit(1)
  end
end

event.listen("modem_message", l_handler)

local delay = 5.0
local i = delay
while not l_found do
  os.sleep(0.05)
  i = i + 0.05
  if i >= delay then
    i = 0
    print("Searching for provisioning system [" .. c_sid .. "]")
    m.broadcast(c_port, "PROVISIONING REQUEST", id, c_sid)
  end
end

print("Provisioning system found")

local filepart = ""
local pidx = nil

l_handler = function(name, localAddress, remoteAddress, port, distance, message, param, param2)
  if math.floor(port) == c_port then
    local hdr = {}
    if l_hdr ~= nil then
      hdr = serialization.unserialize(message)
      message = param
      param = param2
      if hdr["sid"] ~= l_hdr["sid"] then
        return true
      end
    end
    local npidx = nil
    if message == "FILEPART" then
      filepart = filepart .. param
    elseif message == "FILE" then
      param = serialization.unserialize(filepart .. param)
      filepart = ""

      print("Receiving file " .. param["name"])
      local f = io.open(param["name"], "wb")
      f:write(param["data"])
      f:flush()
      f:close()
    elseif message == "DONE" then
      print("Launching questing software")
      event.ignore("modem_message", l_handler)
      l_found = true
    end
    if npidx ~= nil then
      if pidx ~= nil and (pidx+1) ~= npidx then
        print("Packet desync: " .. pidx .. "+1 != " .. npidx)
--        computer.shutdown(true)
      end
      pidx = npidx
    end
  end
end

l_found = false
event.listen("modem_message", l_handler)

while not l_found do
  os.sleep(0.05)
end

shell.setWorkingDirectory("/home")
shell.execute("quest.lua")
os.sleep(5)
computer.shutdown(true)