local fname = ...
local png = dofile("./png.lua")
local pngdraw = dofile("./pngdraw.lua")
local component = require("component")
local gpu = component.gpu

local file = png.loadPNG(fname)
local w, h = gpu.getResolution()
pngdraw.draw(file, 1, 1, w, h*2, true, true)
os.sleep(5)