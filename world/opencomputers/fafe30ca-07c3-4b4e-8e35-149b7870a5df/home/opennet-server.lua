--OpenNet Server
--Internet for OpenComputers

local component = require("component")
local event = require("event")
local keyboard = require("keyboard")

--Config (DNS registration, webpages)

local dns_addr = "<DNS SERVER NETWORK CARD ADDRESS>"
local dns_name = "<YOUR DOMAIN HERE>"
local main_page = "index.onml"

-- Objects (various values needed)

local modem = component.modem
local gpu = component.gpu
local file = io.open(main_page, "r")
local cont = file:read("*a")
file:close()

function init()
  modem.open(55)
  modem.send(dns_addr, 55, "INIT " .. dns_name)
  modem.close(55)
end

function respondToRequest(req)
  if not modem.isOpen(80) then
    modem.open(80)
  end
  print("User with network card address " .. req.remoteAddr .. " requested the main page")
  modem.send(req.remoteAddr, 80, cont)
end

init()
running = true
while running do
  if keyboard.isControlDown() then
    local _, _, _, code, _ = event.pull(1, "key_down")
    if code == keyboard.keys.w then
      running = false
    end
  end
  modem.open(80)
  local _, _, from, _, _, request = event.pull("modem_message")
  os.sleep(0.1)
  respondToRequest({remoteAddr = from, request = request})
end