local bit53

-- As if CC would ever 
if _VERSION == "Lua 5.3" and not term then
    bit53 = {}

    bit53.band = load([[return function(n, m) return n & m end]])()
    bit53.bor = load([[return function(n, m) return n | m end]])()
end

local serialize = require("serialization")
local bit = bit or bit32 or bit53

table.unpack = table.unpack or unpack

local HEADER_CHUNK = {77, 84, 104, 100}
local HEADER_TRACK = {77, 84, 114, 107}

local FORMATS = {
    [0] = "Single track",
    [1] = "Multiple tracks",
    [2] = "Multiple song"
}

local MIDI_EVENT_LEN = {
    [0x80] = 2,
    [0x90] = 2,
    [0xA0] = 2,
    [0xB0] = 2,
    [0xC0] = 1,
    [0xD0] = 1,
    [0xE0] = 2,
    [0xF0] = 2
}

local META_EVENTS = {
    [0x00] = "Sequence number",
    [0x01] = "Text event",
    [0x02] = "Copyright notice",
    [0x03] = "Sequence or track name",
    [0x04] = "Instrument name",
    [0x05] = "Lyric text",
    [0x06] = "Marker text",
    [0x07] = "Cue point",
    [0x20] = "MIDI channel prefix assignment",
    [0x2F] = "End of track",
    [0x51] = "Tempo setting",
    [0x54] = "SMPTE offset",
    [0x58] = "Time signature",
    [0x59] = "Key signature",
    [0x7F] = "Sequencer specific event",
}

local MIDI_SOUND_SET = {
    "Acoustic Grand Piano",
    "Bright Acoustic Piano",
    "Electric Grand Piano",
    "Honky-tonk Piano",
    "Electric Piano 1",
    "Electric Piano 2",
    "Harpsichord",
    "Clavi",
    "Celesta",
    "Glockenspiel",
    "Music Box",
    "Vibraphone",
    "Marimba",
    "Xylophone",
    "Tubular Bells",
    "Dulcimer",
    "Drawbar Organ",
    "Percussive Organ",
    "Rock Organ",
    "Church Organ",
    "Reed Organ",
    "Accordion",
    "Harmonica",
    "Tango Accordion",
    "Acoustic Guitar (nylon)",
    "Acoustic Guitar (steel)",
    "Electric Guitar (jazz)",
    "Electric Guitar (clean)",
    "Electric Guitar (muted)",
    "Overdriven Guitar",
    "Distortion Guitar",
    "Guitar harmonics",
    "Acoustic Bass",
    "Electric Bass (finger)",
    "Electric Bass (pick)",
    "Fretless Bass",
    "Slap Bass 1",
    "Slap Bass 2",
    "Synth Bass 1",
    "Synth Bass 2",
    "Violin",
    "Viola",
    "Cello",
    "Contrabass",
    "Tremolo Strings",
    "Pizzicato Strings",
    "Orchestral Harp",
    "Timpani",
    "String Ensemble 1",
    "String Ensemble 2",
    "SynthStrings 1",
    "SynthStrings 2",
    "Choir Aahs",
    "Voice Oohs",
    "Synth Voice",
    "Orchestra Hit",
    "Trumpet",
    "Trombone",
    "Tuba",
    "Muted Trumpet",
    "French Horn",
    "Brass Section",
    "SynthBrass 1",
    "SynthBrass 2",
    "Soprano Sax",
    "Alto Sax",
    "Tenor Sax",
    "Baritone Sax",
    "Oboe",
    "English Horn",
    "Bassoon",
    "Clarinet",
    "Piccolo",
    "Flute",
    "Recorder",
    "Pan Flute",
    "Blown Bottle",
    "Shakuhachi",
    "Whistle",
    "Ocarina",
    "Lead 1 (square)",
    "Lead 2 (sawtooth)",
    "Lead 3 (calliope)",
    "Lead 4 (chiff)",
    "Lead 5 (charang)",
    "Lead 6 (voice)",
    "Lead 7 (fifths)",
    "Lead 8 (bass + lead)",
    "Pad 1 (new age)",
    "Pad 2 (warm)",
    "Pad 3 (polysynth)",
    "Pad 4 (choir)",
    "Pad 5 (bowed)",
    "Pad 6 (metallic)",
    "Pad 7 (halo)",
    "Pad 8 (sweep)",
    "FX 1 (rain)",
    "FX 2 (soundtrack)",
    "FX 3 (crystal)",
    "FX 4 (atmosphere)",
    "FX 5 (brightness)",
    "FX 6 (goblins)",
    "FX 7 (echoes)",
    "FX 8 (sci-fi)",
    "Sitar",
    "Banjo",
    "Shamisen",
    "Koto",
    "Kalimba",
    "Bag pipe",
    "Fiddle",
    "Shanai",
    "Tinkle Bell",
    "Agogo",
    "Steel Drums",
    "Woodblock",
    "Taiko Drum",
    "Melodic Tom",
    "Synth Drum",
    "Reverse Cymbal",
    "Guitar Fret Noise",
    "Breath Noise",
    "Seashore",
    "Bird Tweet",
    "Telephone Ring",
    "Helicopter",
    "Applause",
    "Gunshot"
}

local MIDI_SOUND_DRUMS = {
    -- https://www.midi.org/specifications/item/gm-level-1-sound-set
}

local readByte

if not term then
    function readByte(fh)
        return string.byte(fh:read(1))
    end

else
    function readByte(fh)
        return fh.read()
    end
end

local function readShort(fh)
    return readByte(fh) * 256 + readByte(fh)
end

local function readLong(fh)
    return readByte(fh) * 15777216 + readByte(fh) * 65526 + readByte(fh) * 256 + readByte(fh)
end

local function readVariableLength(fh)
    local res = 0
    local count = 0

    for i = 1, 4 do
        local byte = readByte(fh)

        count = count + 1
        res = res * 128 + bit.band(byte, 127)

        if byte < 128 then
            return res, count
        end
    end

    return res
end

local function verifyHeader(fh, target)
    for i = 1, #target do
        local byte = readByte(fh)

        if byte ~= target[i] then
            return false
        end
    end

    return true
end

local function midi2freq(pitch)
    return 440 * 2^((pitch - 69) / 12)
end

local function midiToSequence(fn, usingNoteblock)
    local res = {}
    local fh

    if not term then
        fh = io.open(fn, "rb")

    else
        fh = fs.open(fn, "rb")
    end

    if not fh then
        error("No such file '" .. fn .. "'")
    end

    local valid = verifyHeader(fh, HEADER_CHUNK)

    if not valid then
        error("Invalid midi header")

        return false
    end

    local headerLength = readLong(fh)
    local format = readShort(fh)
    local trackChunks = readShort(fh)
    local division = readShort(fh)

    res.header = {
        format = format,
        division = division,
        tracks = trackChunks,
    }

    res.tracks = {}

    if format ~= 0 and format ~= 1 then
        error("Unsupported midi format " .. tostring(format))
    end

    local tempos = {{0, 500000}} -- {{time, tempo}}, 500000 is a midi default

    for trackChunk = 1, trackChunks do
        res.tracks[trackChunk] = {}
        res.tracks[trackChunk].sequence = {}
        res.tracks[trackChunk].header = {}

        local sequence = res.tracks[trackChunk].sequence
        local trackHeaders = res.tracks[trackChunk].header

        local valid = verifyHeader(fh, HEADER_TRACK)

        local length = readLong(fh)
        local count = 0

        local time = 0

        local channels = {}

        local runningStatus = false
        local lastTempo = 1

        while true do
            local tempo

            for i = lastTempo, #tempos do
                if tempos[i][1] > time then
                    break
                end

                lastTempo = i
                tempo = tempos[i][2]
            end

            local ticks, numBytes = readVariableLength(fh)
            local delayTime = (ticks / division) * (tempo / 1000) -- In ms
            local eventType = readByte(fh)

            time = time + math.floor(delayTime + 0.5)

            if eventType >= 0xF0 and eventType <= 0xF7 then
                runningStatus = false
            end

            if eventType == 0xFF then
                local metaType = readByte(fh)
                local metaLength = readVariableLength(fh)

                local data = {}

                for i = 1, metaLength do
                    table.insert(data, readByte(fh))
                end

                -- Tempo change
                if metaType == 0x51 then
                    local tempo = 0

                    for i = 1, #data do
                        tempo = tempo * 256 + data[i]
                    end

                    -- Suggest a more noteblock friendly tempo
                    if usingNoteblock then
                        -- 400000 is 10ticks/s, this code is wrong.
                        -- 400000 -> 500000
                        -- 150bpm base, not 120

                        local ex = math.floor(math.log(60000000 / (tempo * 18.75)) / math.log(2))
                        local newTempo = 60000000 / (2^ex * 18.75)

                        -- print("Tempo " .. tostring(tempo) .. " is not very noteblocky, setting to " .. newTempo)

                        tempo = newTempo
                    end

                    table.insert(tempos, {time, tempo})

                -- Time signature
                elseif metaType == 0x58 then
                    local num = data[1]
                    local den = data[2]
                    local clocks = data[3]

                -- End of track
                elseif metaType == 0x2F then
                    break

                else
                    -- print(tostring(metaType) .. " - " .. META_EVENTS[metaType] .. " - " .. string.byte(table.unpack(data)))
                end

            elseif eventType == 0xF0 or eventType == 0xF7 then
                local metaLength = readVariableLength(fh)

                local data = {}

                for i = 1, metaLength do
                    table.insert(data, readByte(fh))
                end

            else
                -- Midi event

                local bytes = {}

                if eventType < 0x80 then
                    table.insert(bytes, eventType)

                    eventType = runningStatus
                end

                local key = bit.band(eventType, 0xF0)
                local len = MIDI_EVENT_LEN[key] or 0

                while #bytes < len do
                    table.insert(bytes, readByte(fh))
                end

                local channel = bit.band(eventType, 15)

                -- Channel off
                if eventType >= 0x80 and eventType <= 0x8F then
                    local pitch = bytes[1]
                    local velocity = bytes[2]

                    local data = channels[channel][pitch]

                    local start = data[3]
                    local dt = time - start
                    local pitch = data[1]
                    local velocity = 0x40

                    table.insert(sequence, {start, pitch, dt, channel, velocity})

                    -- Yield OC/CC to not crash with Too long without yielding
                    if #sequence % 2^8 == 0 then
                        os.sleep(0)
                    end

                -- Channel on
                elseif eventType >= 0x90 and eventType <= 0x9F then
                    local pitch = bytes[1]
                    local velocity = bytes[2]

                    if not channels[channel] then
                        channels[channel] = {}
                    end

                    if velocity > 0 then
                        channels[channel][pitch] = {pitch, velocity, time}

                    else
                        local data = channels[channel][pitch]

                        local start = data[3]
                        local dt = time - start
                        local pitch = data[1]
                        local velocity = 0x40

                        table.insert(sequence, {start, pitch, dt, channel, velocity})
                    end

                -- Polyphonic aftertouch
                elseif eventType >= 0xA0 and eventType <= 0xAF then
                    local pitch = bytes[1]
                    local pressure = bytes[2]

                -- Control mode change
                elseif eventType >= 0xB0 and eventType <= 0xBF then
                    local byte1 = bytes[1]
                    local byte2 = bytes[2]

                -- Program change
                elseif eventType >= 0xC0 and eventType <= 0xCF then
                    local program = bytes[1]

                    if channel ~= 10 then
                        if not trackHeaders.voice then
                            trackHeaders.voice = {}
                        end

                        trackHeaders.voice[channel] = {name = MIDI_SOUND_SET[program + 1], program = program + 1, percussion = false}
                    end

                -- Channel aftertouch
                elseif eventType >= 0xD0 and eventType <= 0xDF then
                    local pressure = bytes[1]

                -- Pitch wheel range
                elseif eventType >= 0xE0 and eventType <= 0xEF then
                    local lsb = bytes[1]
                    local msb = bytes[2]

                else
                    print("Unknown event " .. tostring(eventType))
                end

                runningStatus = eventType
            end
        end
    end

    if not term then
        fh:close()

    else
        fh.close()
    end

    return res
end

return {midiToSequence = midiToSequence}