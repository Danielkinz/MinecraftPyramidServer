-- ADSR per voice
-- ADSR for slurs
-- Crescendo support for long notes, let the note update its volume during play

local args = {...}

local component
local serialize = require("serialization")
local midi = require("midi")

if not term then
    component = require("component")
end

table.unpack = table.unpack or unpack

-- Velocities from ABC documentation
-- !pppp! = !ppp! = 30, !pp! = 45, !p! = 60, !mp! = 75, !mf! = 90, !f! = 105, !ff! = 120, !fff! = !ffff! = 127.
local DYNAMIC_VOLUMES = {
    ["pppp"] = 30 / 127,  
    ["ppp"] = 30 / 127,
    ["pp"] = 45 / 127,
    ["p"] = 60 / 127,
    ["mp"] = 75 / 127,
    ["mf"] = 90 / 127,
    ["f"] = 105 / 127,
    ["ff"] = 120 / 127,
    ["fff"] = 127 / 127,
    ["ffff"] = 127 / 127
}

-- Values from NBS
local IRON_NOTEBLOCK_INSTRUMENTS = {
    Harpsichord = "harp",
    Strings = "guitar",
    Rototoms = "snare",
    Electric = "flute",
    Treble = "bass",

    -- Midi program numbers
    [101] = "harp",
    [1] = "harp",
    [30] = "guitar",
    [31] = "guitar",
    [33] = "bass",
    [34] = "bass",
    [35] = "bass",
    [39] = "bass",
    [46] = "harp",
    [49] = "harp",
    [49] = "harp",
    [50] = "harp",
    [55] = "harp",
    [69] = "flute",
    [6] = "harp",
    [74] = "flute",
    [81] = "harp",
    [82] = "harp",
    [8] = "harp",
    [99] = "harp",
    [5] = "harp",
    [61] = "harp",
    [9] = "bell",
    [12] = "bell",
    [52] = "harp",
    [70] = "flute",
    [26] = "guitar",
    [25] = "guitar",
    [73] = "flute",
    [22] = "harp",
    [71] = "flute",
    [47] = "harp",
    [79] = "flute",
    [20] = "harp",
    [45] = "harp",
    [36] = "bass",
    [62] = "harp",
    [11] = "bell",
    [75] = "flute",
    [89] = "harp",
    [100] = "harp",
    [48] = "snare",
    [56] = "harp",
    [17] = "bass",
    [40] = "bass"
}

-- Values from NBS
local IRON_NOTEBLOCK_PERCUSSION = {
    [35] = {"basedrum", 10}, -- E4
    [36] = {"basedrum", 6}, -- C4
    [37] = {"hat", 6}, -- C4
    [38] = {"snare", 8}, -- D4
    [40] = {"snare", 4}, -- A#3
    [41] = {"basedrum", 6}, -- C4
    [42] = {"snare", 22}, -- E5
    [43] = {"basedrum", 13}, -- G4
    [44] = {"snare", 22}, -- E5
    [45] = {"basedrum", 15}, -- A4
    [46] = {"snare", 18}, -- C5
    [47] = {"basedrum", 20}, -- D5
    [48] = {"basedrum", 23}, -- F5
    [49] = {"snare", 17}, -- B4
    [51] = {"snare", 24}, -- F#5
    [52] = {"snare", 8}, -- D4
    [53] = {"snare", 13}, -- G4
    [54] = {"hat", 18}, -- C5
    [55] = {"snare", 18}, -- C5
    [57] = {"snare", 13}, -- G4
    [59] = {"snare", 13}, -- G4
    [67] = {"hat", 8}, -- D4
    [69] = {"hat", 20}, -- D5
    [70] = {"hat", 23}, -- F5
}

-- Open for changes... please save me D:
-- Default to Piano if no other name is given
local VOICES_ADSR = {
    --Piano = {50, 0.95, 0.05, 25},
    Piano = {0, 0.95, 0.05, 0},

    Flute = {0, 1, 1, 0},

    Violin = {0, 1, 1, 0},
    Violincello = {0, 1, 1, 0},
}

-- Credits to Odd Stråbø
-- Taken from https://bitbucket.org/Oddstr13/ac-get-repo/src/8a08af2a87b246e200e83a0e2e7f35a3537d0378/tk-lib-str/lib/str.lua
function split(s, sSeparator, nMax, bRegexp)
  -- http://lua-users.org/wiki/SplitJoin
  -- "Function: true Python semantics for split"
  assert(sSeparator ~= '')
  assert(nMax == nil or nMax >= 1)

  local aRecord = {}

  if s:len() > 0 then
    local bPlain = not bRegexp
    nMax = nMax or -1

    local nField=1 nStart=1
    local nFirst,nLast = string.find(s.."",sSeparator, nStart, bPlain)
    while nFirst and nMax ~= 0 do
      aRecord[nField] = string.sub(s.."",nStart, nFirst-1)
      nField = nField+1
      nStart = nLast+1
      nFirst,nLast = string.find(s.."",sSeparator, nStart, bPlain)
      nMax = nMax-1
    end
    aRecord[nField] = string.sub(s.."",nStart)
  end

  return aRecord
end

local function channelDebug()
    term.clear()
    term.setCursor(1, 1)
    term.write("Channel, pitch, start, stop, volume, voice")

    term.setCursor(1, 2)
    term.write("---------------------------")

    for i = 1, #channels do
        term.setCursor(1, i + 2)
        term.write(tostring(i) .. ", " .. tostring(channels[i].pitch or -1) .. ", " .. tostring(channels[i].start or -1) .. ", " .. tostring(channels[i].stop or -1) .. ", " .. tostring(channels[i].volume or -1) .. ", " .. tostring(channels[i].voice or -1))
    end
end

local function midi2freq(pitch)
    return 440 * 2^((pitch - 69) / 12)
end

-- Converts Note block ticks (0-24) to MIDI code (34-58) and vice-versa
-- Based on https://github.com/MightyPirates/OpenComputers/blob/917befcd0e8e256bc52abb038f86bc04f236645a/src/main/resources/assets/opencomputers/loot/openos/lib/note.lua
function ticks(n, force)
    if type(n) == "number" then
        tick = n - 34

        if tick >= 0 and tick <= 24 then
            return tick
        end

        if force then
            for i = -3, 3 do
                new = tick + 12 * i

                if new >= 0 and new <= 24 then
                    return new
                end
            end

            return false

        else
            return false
        end
    end
end

local function pitchChannelIndex(channels, pitch)
    for i = 1, #channels do
        if channels[i].pitch == pitch then
            return true, i
        end
    end

    return false, -1
end

local function getFreeChannel(channels)
    return pitchChannelIndex(channels, -1)
end

local function processChannels(channels, time, soundPool)
    -- Add crescendo support

    local delayTime = time - channels.global.time

    for i = 1, #channels do
        local channel = channels[i]
        local sound = channel.soundCard
        local inUse = channel.inUse

        local nextUpdate = channel.stop - channels.global.time

        if nextUpdate > 0 and pitch ~= -1 then
            delayTime = math.min(delayTime, nextUpdate)

        elseif inUse then
            channel.pitch = -1
            channel.start = -1
            channel.stop = -1
            channel.duration = -1
            channel.volume = -1
            channel.voice = -1
            channel.inUse = false

            if channel.typ == "sound" then
                sound.close(channel.cardChannel)
            end
        end
    end

    if delayTime > 0 and delayTime < math.huge then
        local lastSound

        for j = 1, #soundPool do
            local sound = soundPool[j]

            local card, typ = table.unpack(sound)

            if typ == "sound" then
                card.delay(delayTime)
            end
        end

        channels.global.time = channels.global.time + delayTime

        return true
    end

    return false
end

local function addVoice(voices, voice, songMeta)
    local adsr = VOICES_ADSR.Piano

    if songMeta.voice and songMeta.voice[voice] then
        adsr = VOICES_ADSR[songMeta.voice[voice].name]
    end

    voices[voice] = {
        volume = DYNAMIC_VOLUMES["mp"],

        crescendoStartTime = -1,
        crescendoStopTime = -1,
        crescendoStartVolume = -1,
        crescendoStopVolume = -1,

        diminuendoStartTime = -1,
        diminuendoStopTime = -1,
        diminuendoStartVolume = -1,
        diminuendoStopVolume = -1,

        adsr = adsr
    }
end

function processDecorations(channels, voices, pitch, time, duration, voice, velocity, decorations, songMeta)
    voices[voice].volume = velocity / 127
end

local function playNote(channels, voices, pitch, time, duration, voice, velocity, decorations, songMeta, soundPool)
    while processChannels(channels, time, soundPool) do

    end

    local freeChannel, index = getFreeChannel(channels)

    if freeChannel then
        local channel = channels[index]
        local sound = channel.soundCard

        local voiceProgram = songMeta.voice and songMeta.voice[voice] and songMeta.voice[voice].program
        local voiceName = songMeta.voice and songMeta.voice[voice] and songMeta.voice[voice].name

        local instrument
        local tick

        if voice == 9 then
            instrument, tick = table.unpack(IRON_NOTEBLOCK_PERCUSSION[pitch] or {})

            if not instrument then
                 -- print("New percussion " .. tostring(pitch))
            end

        else
            instrument = IRON_NOTEBLOCK_INSTRUMENTS[voiceProgram] or IRON_NOTEBLOCK_INSTRUMENTS[voiceName]
            tick = ticks(pitch, true)

            if not instrument and (voiceProgram or voiceName) then
                -- print("New instrument " .. tostring(voiceProgram) .. ", " .. tostring(voiceName))
            end
        end

        channel.pitch = pitch
        channel.start = time
        channel.stop = time + duration
        channel.duration = duration

        channel.volume = voices[voice].volume
        channel.voice = voice

        channel.inUse = true

        if channel.typ == "sound" then
            local freq = midi2freq(pitch)

            local a, d, s, r = table.unpack(voices[voice].adsr or VOICES_ADSR["Piano"])

            sound.open(channel.cardChannel)

            sound.setFrequency(channel.cardChannel, freq)
            sound.setADSR(channel.cardChannel, a, d * duration, s, r)
            sound.setVolume(channel.cardChannel, channel.volume)

        elseif channel.typ == "speaker" then
            if tick then
                sound.playNote(instrument or "harp", volume, tick)
            end

        elseif channel.typ == "iron_noteblock" then
            if tick then
                sound.playNote(instrument or "harp", tick, volume)
            end
        end

    else    
        -- Sane error that tells user to install more sound cards
    end
end

local function initChannels()
    local channels = {
        global = {
            time = 0,
        }
    }

    local soundPool = {}
    local usingSoundCard = false

    if term then
        local peripherals = peripheral.getNames()

        for i = 1, #peripherals do
            local side = peripherals[i]
            local typ = peripheral.getType(side)

            if typ == "sound" then
                usingSoundCard = true

                table.insert(soundPool, {peripheral.wrap(side), "sound"})

            elseif typ == "speaker" then
                table.insert(soundPool, {peripheral.wrap(side), "speaker"})

            elseif typ == "iron_noteblock" then
                table.insert(soundPool, {peripheral.wrap(side), "iron_noteblock"})
            end
        end

    else
        for addr in component.list("sound") do
            usingSoundCard = true

            table.insert(soundPool, {component.proxy(addr), "sound"})
        end

        if not usingSoundCard then
            for addr in component.list("iron_noteblock") do
                table.insert(soundPool, {component.proxy(addr), "iron_noteblock"})
            end
        end
    end

    for i = 1, #soundPool do
        local sound, typ = table.unpack(soundPool[i])
        
        if typ == "sound" then
            sound.clear()
            sound.setTotalVolume(1)

            local channelCount = sound.channel_count or sound.getChannelCount()
            local modes = sound.modes or sound.getModes()

            for j = 1, channelCount do
                sound.resetEnvelope(j)
                sound.resetAM(j)
                sound.resetFM(j)

                sound.setWave(j, modes.square)
                sound.setVolume(j, 1)
                sound.close(j)

                channels[#channels + 1] = {
                    soundCard = sound,
                    cardChannel = j,
                    pitch = -1,
                    start = -1,
                    stop = -1,
                    duration = -1,
                    volume = -1,
                    voice = -1,
                    typ = typ,
                    inUse = false
                }
            end

            while not sound.process() do
                os.sleep(0.2)
            end

        elseif typ == "speaker" or typ == "iron_noteblock" then
            for j = 1, 8 do
                channels[#channels + 1] = {
                    soundCard = sound,
                    cardChannel = j,
                    pitch = -1,
                    start = -1,
                    stop = -1,
                    duration = -1,
                    volume = -1,
                    voice = -1,
                    typ = typ,
                    inUse = false,
                }
            end
        end
    end

    return channels, usingSoundCard, soundPool
end

local function mergeVoices(tracks)
    local voices = {}

    for i = 1, #tracks do
        track = tracks[i]

        for k, v in pairs(track.header.voice or {}) do
            voices[k] = v
        end
    end

    return voices
end

local function mergeTracks(tracks)
    local seq = {}

    for i = 1, #tracks do
        local track = tracks[i]

        for j = 1, #track.sequence do
            table.insert(seq, track.sequence[j])
        end
    end

    table.sort(seq, function(a, b) return a[1] < b[1] end)

    return seq
end

local function loadSequence(fn)
    -- TBI
    for line in io.lines(fn) do
        if i == 1 then
            songMeta = serialize.unserialize(line)
        end

        local data = split(line, ";")

        if #data > 4 then
            local time = tonumber(data[1])
            local pitch = tonumber(data[2])
            local duration = tonumber(data[3])
            local voice = tonumber(data[4])
            local decorations = serialize.unserialize(data[5]) or {}
        end
    end
end

local function playSong(song, channels)
    local timeAcc = 0
    local noteAcc = 0

    local songMeta = song.header
    songMeta.voice = mergeVoices(song.tracks)

    local voices = {}

    local channels, usingSoundCard, soundPool = initChannels()

    local sequence = mergeTracks(song.tracks)

    local lastSound = false
    local lastTime = 0
    local lastSleep = os.clock()
    local excessSleep = 0

    for seqId = 1, #sequence do
        local data = sequence[seqId]

        local time = tonumber(data[1])
        local pitch = tonumber(data[2])
        local duration = tonumber(data[3])
        local voice = tonumber(data[4])
        local velocity = tonumber(data[5])

        local decorations = {}

        if not voices[voice] then
            addVoice(voices, voice, songMeta)
        end

        processDecorations(channels, voices, pitch, time, duration, voice, velocity, decorations, songMeta)
        playNote(channels, voices, pitch, time, duration, voice, velocity, durations, songMeta, soundPool)

        if not usingSoundCard and lastTime ~= time then
            local dt = (time - lastTime) / 1000 - os.clock() + lastSleep + excessSleep

            if dt > 0.05 then
                os.sleep(dt)

                excessSleep = dt % 0.05
                lastTime = time
                lastSleep = os.clock()
            end
        end

        if (time - timeAcc > 1000) or noteAcc >= 512 then
            for j = 1, #soundPool do
                local sound = soundPool[j]

                local card, typ = table.unpack(sound)

                if typ == "sound" then
                    while not card.process() do
                        os.sleep(0)
                    end
                end

                timeAcc = time
                noteAcc = 2
            end

        else
            noteAcc = noteAcc + 2
        end
    end

    -- Process the rest of the queue
    for j = 1, #soundPool do
        local sound = soundPool[j]

        local card, typ = table.unpack(sound)

        if typ == "sound" then
            while not card.process() do
                os.sleep(1)
            end
        end
    end
end

local t = os.clock()

print("Loading midi...")

local sequence = midi.midiToSequence(args[1])

print("Playing song...")

playSong(sequence, channels)

print("Played for " .. tostring(os.clock() - t))