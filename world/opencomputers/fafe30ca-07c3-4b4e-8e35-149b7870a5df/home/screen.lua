local comp = require("component")
local event = require("event")
local gpu = comp.gpu
local term = require("term")

while true do

  term.clear()

  gpu.setForeground(0X2196f3)
  print("")
  print("Objective one:")
  gpu.setForeground(0Xf44336)
  print("    Incomplete")
  
  gpu.setForeground(0X2196f3)
  print("Objective two:")
  gpu.setForeground(0X64dd17)
  print("    Complete")

  gpu.setForeground(0XFFFFFF)
  gpu.setBackground(0X000000)
  
os.sleep(30)
end