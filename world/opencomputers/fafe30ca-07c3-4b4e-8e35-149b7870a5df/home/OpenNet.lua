-- OpenNet: A browser for the OpenNet

local component = require("component")
local event = require("event")
local term = require("term")

-- Config (you can change this)

local dns_addr = "<DNS SERVER NETWORK CARD ADDRESS>"

local modem = component.modem

print("What URL would you like to go to?")
local url = io.read()

function string.starts(String,Starts)
  return string.sub(String,1,String.len(Starts))==Starts
end

function lines(str)
  local t = {}
  local function helper(line) table.insert(t, line) return "" end
  helper((str:gsub("(.-)\n", helper)))
  return t
end

function parseColor(tln)
  if tln == "RED" then return 0xFF0000
  elseif tln == "ORA" then return 0xFF6600
  elseif tln == "YEL" then return 0xFFFF00
  elseif tln == "GRE" then return 0x00FF00
  elseif tln == "BLU" then return 0x0000FF
  elseif tln == "PUR" then return 0xFF00FF
  elseif tln == "WHI" then return 0xFFFFFF
  elseif tln == "BLA" then return 0x000000 end
end
local gpu = component.gpu
function parseOnml(onml)
  local lns = lines(onml)
  for i,line in ipairs(lns) do
    if string.starts(line, "BACK") then
      gpu.setBackground(parseColor(string.sub(line, 6)))
      local w,h= gpu.getResolution()
      gpu.fill(1, 1, w, h, " ")
    else
      gpu.setForeground(parseColor(string.sub(line, 1, 3)))
      term.write(string.sub(line,4).."\n")
    end
  end
  gpu.setForeground(0xFFFFFF)
end

modem.open(55)
modem.send(dns_addr, 55, "FIND " .. url)
local _, _, _, _, _, resp = event.pull("modem_message")
print("Pinging server at " .. url .. "...")
modem.close(55)
modem.open(80)
term.clear()
term.setCursor(1,1)
modem.send(resp, 80, "GET")
local _, _, _, _, _, resp2 = event.pull("modem_message")
parseOnml(resp2)
gpu.setBackground(0x000000)