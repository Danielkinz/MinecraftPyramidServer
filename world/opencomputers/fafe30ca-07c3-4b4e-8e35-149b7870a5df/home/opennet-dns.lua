-- OpenNet DNS: DNS for OpenNet

local component = require("component")
local event = require("event")
local m = component.modem
local domains = {}

m.open(55)

function string.starts(String,Start)
   return string.sub(String,1,string.len(Start))==Start
end

while true do
  local _, _, from, port, _, message = event.pull("modem_message")
  if string.starts(message,"INIT") then
    print("Registering domain " .. string.sub(message, 6) .. " with address " .. from)
    domains[string.sub(message,6)] = from
  elseif string.starts(message,"FIND") then
    print("Finding " .. string.sub(message,6) .. " for computer with network card address " .. from)
    os.sleep(0.1)
    m.send(from, 55, domains[string.sub(message,6)])
  end
end