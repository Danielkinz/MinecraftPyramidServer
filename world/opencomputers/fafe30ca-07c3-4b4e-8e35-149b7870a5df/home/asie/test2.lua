local ocpng = dofile("./png.lua")
local pngdraw = dofile("./pngdraw.lua")
local component = require("component")
local event = require("event")
local os = require("os")
local sides = require("sides")
local gpu = component.gpu
local unicode = require("unicode")
local halfchar = unicode.char(0x2584)
local stepchars = {}
for i=1,8 do
  stepchars[i] = unicode.char(0x2580 + i)
end

function drawPNG(png, xp, yp)
  if png == nil then
    gpu.setBackground(0xFF00FF)
    gpu.fill(xp, yp, 16, 8, " ")
    gpu.setBackground(0x000000)
    gpu.fill(xp + 8, yp, 8, 4, " ")
    gpu.fill(xp, yp + 4, 8, 4, " ")
    return
  end
  pngdraw.draw(png, xp, yp, 16, 16, true, false)
end

function drawFluid(req, xp, yp)
  local level = req.currCount * 8.0 / req.count
  if level > 8 then level = 8 end
  local levelI, levelF = math.modf(level)
  local levelFC = math.floor(levelF * 8)
  local brY = 8 - levelI
  gpu.setBackground(0x333333)
  gpu.setForeground(req.color)
  gpu.fill(xp, yp, 16, 8, " ")
  if brY < 8 then
    gpu.fill(xp, yp + brY, 16, 8 - brY, stepchars[8])
  end
  if levelFC > 0 then
    gpu.fill(xp, yp + brY - 1, 16, 1, stepchars[levelFC])
  end
end

function drawReq(req, xp, yp, ticks)
  if req.type == "item" and ticks == 0 then
    drawPNG(req.png, xp, yp)
  elseif req.type == "fluid" or req.type == "energy" then
    drawFluid(req, xp, yp)
  end
end

local reqs = {}
local reqFile = io.open("req2.txt", "r")
local i = 1
local line = reqFile:read()
local reqSpPos = line:find(" ")
local reqX = tonumber(line:sub(0, reqSpPos - 1))
local reqY = tonumber(line:sub(reqSpPos + 1))

while true do
  line = reqFile:read()
  if line == nil then break end
  local data = {}
  local uname = ""
  local j = 1
  local jmode = 0
  for v in string.gmatch(line, "%S+") do
    if jmode == 0 then
      if v == "::" then
        jmode = 1
        j = 1
      else
        data[j]=v
      end
    else
      if j == 1 then
        uname = v
      else
        uname = uname .. " " .. v
      end
    end
    j = j + 1
  end
  reqs[i] = {}
  reqs[i].fname = uname
  reqs[i].type = data[2]
  reqs[i].count = tonumber(data[1])
  reqs[i].filter = {}
  if reqs[i].type == "item" then
    reqs[i].filter.name = data[3]
    local dmgpos = data[3]:find("/")
    if dmgpos ~= nil then
      reqs[i].filter.damage = tonumber(data[3]:sub(dmgpos + 1))
      reqs[i].filter.name = data[3]:sub(0, dmgpos - 1)
    end
    reqs[i].icon = data[3]:gsub("[:/]", "_")
  elseif reqs[i].type == "fluid" then
    reqs[i].filter = data[3]
    reqs[i].color = tonumber(data[4], 16)
  elseif reqs[i].type == "energy" then
    reqs[i].filter = data[3]
    reqs[i].color = 0xFF0000
  end
  if reqs[i].type == "item" then
    local fname = "images/" .. reqs[i].icon .. ".png"
    local f = io.open(fname, "r")
    if f ~= nil then
      io.close(f)
      reqs[i].png = ocpng.loadPNG(fname)
    else
      reqs[i].png = nil
    end
  end
  i = i + 1
end

local function centerPos(x, w)
  return (w - x) / 2
end

local function reqItemMatch(item, req)
  if item.name ~= req.filter.name then return false end
  if req.filter.damage ~= nil and item.damage ~= req.filter.damage then return false end
  return true
end

local function reqFluidMatch(fluid, req)
  return fluid.name == req.filter
end

local itemSlots = {}
local fluidSlots = {}
local energySlots = {}

local proxyCache = {}
local function getComponent(addr)
  if proxyCache[addr] ~= nil then return proxyCache[addr] end
  local ctr = component.proxy(addr)
  proxyCache[addr] = ctr
  return ctr
end

local function scanInventories()
  itemSlots = {}
  fluidSlots = {}
  energySlots = {}
  local kItem = 1
  local kFluid = 1
  for addr, ctype in component.list("transposer") do
    local ctr = getComponent(addr)
    for side = 0,5 do
      local size = ctr.getInventorySize(side)
      if size ~= nil and size > 0 then
        for i=1,size do
          itemSlots[kItem] = ctr.getStackInSlot(side, i)
          kItem = kItem + 1
        end
      end

      local fluid = ctr.getFluidInTank(side)
      for i = 1,fluid.n do
        fluidSlots[kFluid] = fluid[i]
        kFluid = kFluid + 1
      end
    end
  end
  for addr, ctype in component.list("energy_device") do
print(addr)
    local ctr = getComponent(addr)
    energySlots[addr] = ctr.getEnergyStored()
    print(ctr)
    print(ctr.getEnergyStored())
  end
end

local function getReqCountFulfilled(req)
  local cnt = 0
  if req.type == "item" then
    for i,item in ipairs(itemSlots) do
      if reqItemMatch(item, req) then cnt = cnt + item.size end
    end
  elseif req.type == "fluid" then
    for i,fluid in ipairs(fluidSlots) do
      if reqFluidMatch(fluid, req) then cnt = cnt + fluid.amount end
    end
  elseif req.type == "energy" then
    if req.filter == "*" then
      for i,energy in pairs(energySlots) do
        print("Alll" .. cnt + " E" .. energy)
        cnt = cnt + energy
      end
    else
      if energySlots[req.filter] ~= nil then cnt = cnt + energySlots[req.filter] end
    end
  end
  return math.floor(cnt)
end

component.modem.setStrength(256)

local lastOutput = nil
local function setRedstoneOutput(val)
  if lastOutput ~= val then
    component.modem.broadcast(7777, "match_value", val)
    lastOutput = val
  end
end

local x = 2
local y = 2
local w = 30
local h = 15
local bgColor = 0x333333

-- local scrW, scrH = gpu.getResolution()
local xOff = x
local yOff = y
local scrW = xOff * 2 + w * reqX
local scrH = yOff * 2 + h * reqY - 2

gpu.setResolution(scrW, scrH)
gpu.setBackground(bgColor)
gpu.fill(1, 1, scrW, scrH, " ")

local running = true
local runListener
runListener = function(name,addr,char,key,player)
  running = false
  event.ignore("key_down", runListener)
end
event.listen("key_down", runListener)
local ticks = 0

while running do
local matched = true

scanInventories()

for i, req in ipairs(reqs) do
  local fCount = getReqCountFulfilled(req)
  local info = req.fname .. " (" .. fCount .. "/" .. req.count .. ")"
  req.currCount = fCount
  if fCount < req.count then
    matched = false
  end

  gpu.setBackground(bgColor)
  local yc = y + centerPos(12, h)
  gpu.fill(x, yc + 11, w, 1, " ")

  if fCount < req.count then
    gpu.setBackground(0xCC3333)
  else
    gpu.setBackground(0x33CC33)
  end
  gpu.fill(x + centerPos(20, w), yc, 20, 1, " ")
  gpu.fill(x + centerPos(20, w), yc, 2, 10, " ")
  gpu.fill(x + centerPos(20, w), yc + 9, 20, 1, " ")
  gpu.fill(x + centerPos(20, w) + 18, yc, 2, 10, " ") 
  drawReq(req, x + centerPos(20, w) + 2, yc + 1, ticks)

  gpu.setBackground(bgColor)
  gpu.setForeground(0xFFFFFF)
  gpu.set(x + centerPos(#info, w) - 1, yc + 11, info)

  x = x + w
  if x+w >= scrW then
    x = xOff
    y = y + h
  end
end

setRedstoneOutput(matched)
os.sleep(0)
x = xOff
y = yOff
ticks = ticks + 1

end

event.ignore("key_down", runListener)