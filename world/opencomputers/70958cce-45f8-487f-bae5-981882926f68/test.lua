local component = require("component")
local sides = require("sides")

local slot = 1
local item = component.inventory_controller.getStackInSlot(0, slot)

if item then
  print ("Item Name:", item.name)
  print ("Item count:", item.size)
  print ("Item damage:", item.damage)
else
  print("Slot is empty")
end