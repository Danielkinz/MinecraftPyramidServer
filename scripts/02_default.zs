/**
 * ------------------------------------------------------------
 *
 * This file is part of the FTB Pyramid Reborn Modpack for Minecraft
 * Copyright (c) 2018 Feed The Beast
 *
 * All Rights Reserved unless otherwise explicitly stated.
 *
 * ------------------------------------------------------------
 */

// Raw Carbon Fibre w/ Ore Dict & Charcoal

recipes.remove(<ic2:crafting:13>);

recipes.addShaped(<ic2:crafting:13> * 1, [
  [<ore:dustCoal>, <ore:dustCoal>],
  [<ore:dustCoal>, <ore:dustCoal>]
]);

recipes.addShaped(<ic2:crafting:13> * 1, [
  [<ore:dustCharcoal>, <ore:dustCharcoal>],
  [<ore:dustCharcoal>, <ore:dustCharcoal>]
]);

// Sulfur Removal/Unification

recipes.remove(<ic2:dust:16>);

recipes.addShapeless(<thermalfoundation:material:771> * 1, [
  <ore:dustTinySulfur>, <ore:dustTinySulfur>, <ore:dustTinySulfur>,
  <ore:dustTinySulfur>, <ore:dustTinySulfur>, <ore:dustTinySulfur>,
  <ore:dustTinySulfur>, <ore:dustTinySulfur>, <ore:dustTinySulfur>
]);

// -----------------
//  Extra Utilities
// -----------------

// Remove Deep Dark Portal
recipes.remove(<extrautils2:teleporter:1>);

// Add Pulverized Coal recipe to the Crusher
mods.extrautils2.Crusher.add(<thermalfoundation:material:768>, <minecraft:coal>);

// Add Pulverized Charcoal recipe to the Crusher
mods.extrautils2.Crusher.add(<thermalfoundation:material:769>, <minecraft:coal:1>);

// -----------------------
//  Immersive Engineering
// -----------------------

// Replace Immersive Engineering's Sulfur with TFs'

mods.immersiveengineering.Crusher.removeRecipe(<minecraft:blaze_powder>);
mods.immersiveengineering.Crusher.removeRecipe(<minecraft:dye:4>);
mods.immersiveengineering.Crusher.removeRecipe(<minecraft:quartz>);

mods.immersiveengineering.Crusher.addRecipe(<minecraft:blaze_powder> * 4, <minecraft:blaze_rod>, 3200, <thermalfoundation:material:771>, 0.5);
mods.immersiveengineering.Crusher.addRecipe(<minecraft:dye:4> * 9, <minecraft:lapis_ore>, 6000, <thermalfoundation:material:771>, 0.15);
mods.immersiveengineering.Crusher.addRecipe(<minecraft:quartz> * 3, <ore:oreQuartz>, 6000, <thermalfoundation:material:771>, 0.15);

// -----------
//  Minecraft
// -----------

// Changed item frame recipe to be accessible early game for FTB Achievements
recipes.remove(<minecraft:item_frame>);
recipes.addShaped(<minecraft:item_frame> * 1, [
	[<ore:stickWood>, <ore:stickWood>, <ore:stickWood>],
	[<ore:stickWood>, <ore:plankWood>, <ore:stickWood>],
	[<ore:stickWood>, <ore:stickWood>, <ore:stickWood>]
]);

// Alter recipes for flint
recipes.remove(<minecraft:flint>);

recipes.addShapeless(<minecraft:flint> * 1, [
  <ore:gravel>, <ore:gravel>, <ore:gravel>
]);

recipes.addShapeless(<minecraft:flint> * 9, [
  <tp:flint_block>
]);

// ------------------
//  FTB Achievements
// ------------------

// Powered crafting table
recipes.addShaped(<ftbachievements:poweredcraftingtable> * 1, [
  [<ore:dustRedstone>, <ore:dustRedstone>, <ore:dustRedstone>],
  [<ore:dustRedstone>, <minecraft:crafting_table>, <ore:dustRedstone>],
  [<ore:dustRedstone>, <ore:dustRedstone>, <ore:dustRedstone>]
]);

// ------------------
//  Tiny Progression
// ------------------

recipes.remove(<tp:emerald_multi>);
recipes.addShaped(<tp:emerald_multi> * 1, [
  [<actuallyadditions:item_pickaxe_crystal_green>, <actuallyadditions:item_axe_crystal_green>, <actuallyadditions:item_shovel_crystal_green>],
  [null, <ore:stickWood>, null],
  [null, <ore:stickWood>, null]
]);

// -----------
//  CoFH Mods
// -----------

recipes.remove(<thermalexpansion:augment:258>);

recipes.remove(<thermalfoundation:material:257>);
recipes.addShaped(<thermalfoundation:material:257> * 1, [
	[null, <thermalfoundation:material:129>, null],
	[<thermalfoundation:material:129>, <ore:ingotIron>, <thermalfoundation:material:129>],
	[null, <thermalfoundation:material:129>, null]
]);

recipes.remove(<thermalfoundation:material:256>);
recipes.addShaped(<thermalfoundation:material:256> * 1, [
	[null, <thermalfoundation:material:128>, null],
	[<thermalfoundation:material:128>, <ore:ingotIron>, <thermalfoundation:material:128>],
	[null, <thermalfoundation:material:128>, null]
]);

// --------------------
//  Actually Additions
// --------------------

// Remove obsidian tools, weapons and armour
recipes.remove(<actuallyadditions:item_helm_obsidian>);
recipes.remove(<actuallyadditions:item_chest_obsidian>);
recipes.remove(<actuallyadditions:item_pants_obsidian>);
recipes.remove(<actuallyadditions:item_boots_obsidian>);
recipes.remove(<actuallyadditions:item_sword_obsidian>);
recipes.remove(<actuallyadditions:item_pickaxe_obsidian>);
recipes.remove(<actuallyadditions:item_shovel_obsidian>);
recipes.remove(<actuallyadditions:item_axe_obsidian>);
recipes.remove(<actuallyadditions:item_hoe_obsidian>);

// Fix toast recipe conflict
var knife = <actuallyadditions:item_knife>.anyDamage();
recipes.remove(<actuallyadditions:item_food:10>);
recipes.addShapeless(<actuallyadditions:item_food:10> * 2, [
	knife, <minecraft:bread>
]);

// Remove the need for lava buckets in the recipe to make this recipe possible in the pack
recipes.remove(<actuallyadditions:block_lava_factory_controller>);
recipes.addShaped(<actuallyadditions:block_lava_factory_controller> * 1, [
	[<actuallyadditions:item_misc:7>, <actuallyadditions:block_misc:9>, <actuallyadditions:item_misc:7>],
	[<actuallyadditions:block_crystal:5>, <actuallyadditions:item_misc:7>, <actuallyadditions:block_crystal:5>],
	[<minecraft:bucket>, <minecraft:bucket>, <minecraft:bucket>]
]);
