/**
 * ------------------------------------------------------------
 *
 * This file is part of the FTB Pyramid Reborn Modpack for Minecraft
 * Copyright (c) 2018 Feed The Beast
 *
 * All Rights Reserved unless otherwise explicitly stated.
 *
 * ------------------------------------------------------------
 */

// OreDict additions for Coal Dust

val dustCoal = <ore:dustCoal>;

dustCoal.add(<actuallyadditions:item_dust:6>);
dustCoal.add(<ic2:dust:2>);

// OreDict additions for Sulfur Dust

val dustSulfur = <ore:dustSulfur>;

dustSulfur.add(<immersiveengineering:material:25>);
dustSulfur.add(<ic2:dust:16>);

// OreDict Forestry biomass and unify IC2 version

val bioMass = <ore:bioMass>;

bioMass.add(<forestry:fluid.biomass>);
bioMass.add(<ic2:biomass>);
