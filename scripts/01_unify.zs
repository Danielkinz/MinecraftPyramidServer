/**
 * ------------------------------------------------------------
 *
 * This file is part of the FTB Pyramid Reborn Modpack for Minecraft
 * Copyright (c) 2018 Feed The Beast
 *
 * All Rights Reserved unless otherwise explicitly stated.
 *
 * ------------------------------------------------------------
 */

// Plate hammer crafting
recipes.addShapeless(<thermalfoundation:material:325>, [<ic2:forge_hammer:*>, <ore:ingotNickel>]);
recipes.addShapeless(<thermalfoundation:material:324>, [<ic2:forge_hammer:*>, <ore:ingotAluminum>]);
recipes.addShapeless(<thermalfoundation:material:352>, [<ic2:forge_hammer:*>, <ore:ingotSteel>]);
recipes.addShapeless(<thermalfoundation:material:33>, [<ic2:forge_hammer:*>, <ore:ingotGold>]);
recipes.addShapeless(<thermalfoundation:material:322>, [<ic2:forge_hammer:*>, <ore:ingotSilver>]);
recipes.addShapeless(<thermalfoundation:material:355>, [<ic2:forge_hammer:*>, <ore:ingotBronze>]);
recipes.addShapeless(<thermalfoundation:material:321>, [<ic2:forge_hammer:*>, <ore:ingotTin>]);
recipes.addShapeless(<thermalfoundation:material:320>, [<ic2:forge_hammer:*>, <ore:ingotCopper>]);
recipes.addShapeless(<thermalfoundation:material:32>, [<ic2:forge_hammer:*>, <ore:ingotIron>]);
recipes.addShapeless(<thermalfoundation:material:323>, [<ic2:forge_hammer:*>, <ore:ingotLead>]);
recipes.addShapeless(<thermalfoundation:material:353>, [<ic2:forge_hammer:*>, <ore:ingotElectrum>]);
recipes.addShapeless(<thermalfoundation:material:327>, [<ic2:forge_hammer:*>, <ore:ingotIridium>]);
recipes.addShapeless(<thermalfoundation:material:356>, [<ic2:forge_hammer:*>, <ore:ingotConstantan>]);
recipes.addShapeless(<thermalfoundation:material:358>, [<ic2:forge_hammer:*>, <ore:ingotLumium>]);
recipes.addShapeless(<thermalfoundation:material:359>, [<ic2:forge_hammer:*>, <ore:ingotEnderium>]);
recipes.addShapeless(<thermalfoundation:material:354>, [<ic2:forge_hammer:*>, <ore:ingotInvar>]);
recipes.addShapeless(<thermalfoundation:material:357>, [<ic2:forge_hammer:*>, <ore:ingotSignalum>]);
recipes.addShapeless(<thermalfoundation:material:328>, [<ic2:forge_hammer:*>, <ore:ingotMithril>]);
recipes.addShapeless(<thermalfoundation:material:326>, [<ic2:forge_hammer:*>, <ore:ingotPlatinum>]);
recipes.addShapeless(<thermalfoundation:material:326>, [<immersiveengineering:tool>, <ore:ingotPlatinum>]);
recipes.addShapeless(<thermalfoundation:material:328>, [<immersiveengineering:tool>, <ore:ingotMithril>]);
recipes.addShapeless(<thermalfoundation:material:357>, [<immersiveengineering:tool>, <ore:ingotSignalum>]);
recipes.addShapeless(<thermalfoundation:material:354>, [<immersiveengineering:tool>, <ore:ingotInvar>]);
recipes.addShapeless(<thermalfoundation:material:359>, [<immersiveengineering:tool>, <ore:ingotEnderium>]);
recipes.addShapeless(<thermalfoundation:material:358>, [<immersiveengineering:tool>, <ore:ingotLumium>]);
recipes.addShapeless(<thermalfoundation:material:356>, [<immersiveengineering:tool>, <ore:ingotConstantan>]);
recipes.addShapeless(<thermalfoundation:material:327>, [<immersiveengineering:tool>, <ore:ingotIridium>]);
recipes.addShapeless(<thermalfoundation:material:353>, [<immersiveengineering:tool>, <ore:ingotElectrum>]);
recipes.addShapeless(<thermalfoundation:material:323>, [<immersiveengineering:tool>, <ore:ingotLead>]);
recipes.addShapeless(<thermalfoundation:material:325>, [<immersiveengineering:tool>, <ore:ingotNickel>]);
recipes.addShapeless(<thermalfoundation:material:324>, [<immersiveengineering:tool>, <ore:ingotAluminum>]);
recipes.addShapeless(<thermalfoundation:material:352>, [<immersiveengineering:tool>, <ore:ingotSteel>]);
recipes.addShapeless(<thermalfoundation:material:33>, [<immersiveengineering:tool>, <ore:ingotGold>]);
recipes.addShapeless(<thermalfoundation:material:322>, [<immersiveengineering:tool>, <ore:ingotSilver>]);
recipes.addShapeless(<thermalfoundation:material:321>, [<immersiveengineering:tool>, <ore:ingotTin>]);
recipes.addShapeless(<thermalfoundation:material:320>, [<immersiveengineering:tool>, <ore:ingotCopper>]);
recipes.addShapeless(<thermalfoundation:material:32>, [<immersiveengineering:tool>, <ore:ingotIron>]);

// Ore + Petrotheum now crafts 2 metal dust from TF
recipes.remove(<ic2:dust:2>);
recipes.addShapeless(<thermalfoundation:material:768> * 2, [<minecraft:coal_ore>, <thermalfoundation:material:1027>]);
recipes.remove(<ic2:dust:10>);
recipes.addShapeless(<thermalfoundation:material:67> * 2, [<thermalfoundation:ore:3>, <thermalfoundation:material:1027>]);
recipes.remove(<ic2:dust:8>);
recipes.addShapeless(<thermalfoundation:material> * 2, [<minecraft:iron_ore>, <thermalfoundation:material:1027>]);
recipes.remove(<ic2:dust:7>);
recipes.addShapeless(<thermalfoundation:material:1> * 2, [<minecraft:gold_ore>, <thermalfoundation:material:1027>]);
recipes.remove(<ic2:dust:4>);
recipes.addShapeless(<thermalfoundation:material:64> * 2, [<thermalfoundation:ore>, <thermalfoundation:material:1027>]);
recipes.remove(<ic2:dust:17>);
recipes.addShapeless(<thermalfoundation:material:65> * 2, [<thermalfoundation:ore:1>, <thermalfoundation:material:1027>]);
recipes.remove(<ic2:dust:14>);
recipes.addShapeless(<thermalfoundation:material:66> * 2, [<thermalfoundation:ore:2>, <thermalfoundation:material:1027>]);
recipes.remove(<ic2:dust:12>);
recipes.addShaped(<thermalfoundation:material:770>, [[<ic2:dust:25>, <ic2:dust:25>, <ic2:dust:25>], [<ic2:dust:25>, <ic2:dust:25>, <ic2:dust:25>], [<ic2:dust:25>, <ic2:dust:25>, <ic2:dust:25>]]);

// Remove and hide IC2 biomass
mods.jei.JEI.removeAndHide(<forestry:refractory:1>.withTag({Fluid: {FluidName: "ic2biomass", Amount: 1000}}));
mods.jei.JEI.removeAndHide(<forestry:can:1>.withTag({Fluid: {FluidName: "ic2biomass", Amount: 1000}}));
mods.jei.JEI.removeAndHide(<ic2:fluid_cell>.withTag({Fluid: {FluidName: "ic2biomass", Amount: 1000}}));
mods.jei.JEI.removeAndHide(<thermalexpansion:florb>.withTag({Fluid: "ic2biomass"}));
mods.jei.JEI.removeAndHide(<forge:bucketfilled>.withTag({FluidName: "ic2biomass", Amount: 1000}));
mods.jei.JEI.removeAndHide(<forestry:capsule:1>.withTag({Fluid: {FluidName: "ic2biomass", Amount: 1000}}));
mods.jei.JEI.removeAndHide(<ic2:biomass>);
